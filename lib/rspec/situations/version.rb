module RSpec
	module Situations

		VERSION_NUMBERS = [
			VERSION_MAJOR = 0,
			VERSION_MINOR = 0,
			VERSION_BUILD = 3
		]

		VERSION = VERSION_NUMBERS.join '.'

	end
end
